# Virtuality Workshop Materials

### 1.About the workshop
The Virtuality workshop series (2 workshops) aims to provide the audience with foundational knowledge in virtual reality and hands-on experience in using headsets and developing a virtual reality application. Throughout the workshop series, the audience will gain a foundation knowledge of virtual reality based on the 3 I’s framework (Interaction, Immersion, Imagination) together with the current state-of-the-art developments and applications (head-mounted display - HMD, haptic technologies,...). For the hands-on aspect, the first workshop will guide the audience in experiencing currently available headsets in both the VR Lab and Innovation Maker Space to have a realistic understanding and acknowledge the drawbacks of the current technology. The second workshop will let the audience build their own virtual reality application in Unity and deploy it to the available headsets for testing.

### 2. Format
Workshop 1: Experiencing virtual reality through HMD devices
- 3 sections (8-10 audiences per section)
- For each section: 1 host and 1 facilitator
- Content: Three I’s framework of virtual reality, state-of-the-art development in industry and research, virtual reality application, usage safety requirements, virtual reality experience section.

Workshop 2: Building your first virtual reality application in Unity
- 1 section (24 audiences, 3 audiences per team of development)
- Content: Three I’s framework of virtual reality, virtual reality application, Unity installation (done prior to the workshop), Unity development, deployment, testing.

### 3. Organizing team
- Nguyễn Đại Nghĩa - CS major, CECS
  - Research assistant - Virtual Reality & Hologram Lab
  - Director of Technology - VinMaker Society
- Ngô An Hà Trang - EE major, CECS
  - Research assistant - Virtual Reality & Hologram Lab
  - Director of Technology - VinMaker Society
- Lại Đắc Tiến - EE major, CECS 
  - Research assistant - Virtual Reality & Hologram Lab
  - Maker Team - VinMaker Society

### 4. What's in this repo?
This repo contains materials for both Workshop 1 and Workshop 2, specifically:
- Workshop 1: Workshop slides
- Workshop 2: Workshop slides, template Unity project, final Unity project. The Unity project is adapted from [Veggie Saber](https://www.raywenderlich.com/4912095-veggie-saber-introduction-to-unity-development-with-the-oculus-quest) project with modifications to utilize the OVR prefabs systems from Oculus SDK.
